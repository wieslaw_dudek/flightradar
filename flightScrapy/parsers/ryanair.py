from selenium.webdriver.common.action_chains import ActionChains
from ..items import FlightItem
from .generic import GenericHandler, overrides


class RyanairHandler(GenericHandler):
    carrier = "Ryanair"

    @overrides(GenericHandler)
    def _set_date(self, date, date_xp):
        date_arr = date.split('-')[::-1]
        for i in range(3):
            self._send_keys(date_xp[i], date_arr[i])

    @overrides(GenericHandler)
    def _get_date_from(self):
        return [self.date_from_dd_xp, self.date_from_mm_xp, self.date_from_yyyy_xp]

    @overrides(GenericHandler)
    def _get_date_to(self):
        return [self.date_to_dd_xp, self.date_to_mm_xp, self.date_to_yyyy_xp]

    # xpath to localize
    to_close_xp = '//*[@id="home"]/cookie-pop-up/div/div[2]/core-icon'
    departure_station_xp = '//*[@id="search-container"]/div[1]/div/form/div[2]/div/div/div[1]/div[2]/div[2]/div/div[1]/input'
    arrival_station_xp = '//*[@id="search-container"]/div[1]/div/form/div[2]/div/div/div[3]/div[2]/div[2]/div/div[1]/input'
    submit_button_xp = '//*[@id="search-container"]/div[1]/div/form/div[4]/button[2]'
    two_sides_xp = '//*[@id="lbl-flight-search-type-return"]'

    date_from_dd_xp = '//*[@id="row-dates-pax"]/div[1]/div/div[1]/div/div[2]/div[2]/div/input[1]'
    date_from_mm_xp = '//*[@id="row-dates-pax"]/div[1]/div/div[1]/div/div[2]/div[2]/div/input[2]'
    date_from_yyyy_xp = '//*[@id="row-dates-pax"]/div[1]/div/div[1]/div/div[2]/div[2]/div/input[3]'

    date_to_dd_xp = '//*[@id="row-dates-pax"]/div[1]/div/div[2]/div/div[2]/div[2]/div/input[1]'
    date_to_mm_xp = '//*[@id="row-dates-pax"]/div[1]/div/div[2]/div/div[2]/div[2]/div/input[2]'
    date_to_yyyy_xp = '//*[@id="row-dates-pax"]/div[1]/div/div[2]/div/div[2]/div[2]/div/input[3]'

    # xpath to parse results
    departure_time_xp = './/div[@class="start-time"]'
    arrival_time_xp = './/div[@class="end-time"]'

    times_towards_xp = '(//div[@class="flights-table"])[1]//div[contains(@class,"ranimate-flights-table") and not(contains(@class,"disabled"))]//following-sibling::div[1]/div/div/div[@class="meta-row time"]'
    times_backwards_xp = '(//div[@class="flights-table"])[2]//div[contains(@class,"ranimate-flights-table") and not(contains(@class,"disabled"))]//following-sibling::div[1]/div/div/div[@class="meta-row time"]'

    prices_towards_xp = '(//div[@class="flights-table"])[1]//div[1]/div[@class="flight-header__min-price hide-mobile"]//flights-table-price//span//following-sibling::span[contains(@class,"flights-table-price__price")]'
    prices_backwards_xp = '(//div[@class="flights-table"])[2]//div[1]/div[@class="flight-header__min-price hide-mobile"]//flights-table-price//span//following-sibling::span[contains(@class,"flights-table-price__price")]'
