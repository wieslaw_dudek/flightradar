from .generic import GenericHandler, overrides
import time


class EasyJetHandler(GenericHandler):
    carrier = "EasyJet"

    @overrides(GenericHandler)
    def _set_date(self, date, xp):
        self._click(xp)
        time.sleep(2)
        if xp==self.date_from_xp:
          self._click(self.date_to_click_xp1+date+'"]',True)
        else:
          self._click(self.date_to_click_xp2+date+'"]',True)
        try:
          self._click('//*[@id="close-drawer-link"]')
        except:
          pass

    @overrides(GenericHandler)
    def _get_date_from(self):
        return self.date_from_xp

    @overrides(GenericHandler)
    def _get_date_to(self):
        return self.date_to_xp

    # xpath to localize
    to_close_xp = None
    departure_station_xp = '//input[starts-with(@id,"origin")]'
    arrival_station_xp = ' //input[starts-with(@id,"destination")]'
    submit_button_xp = '//*[@id="pageWrapper"]/main/div/div[1]/section/div[1]/div/div/ul/li[1]/div/div/form/button[1]'
    two_sides_xp = None

    date_from_xp = '//div[@class="outbound-date-picker"]//button[starts-with(@id,"routedatepicker")]'
    date_to_xp = '//div[@class="return-date-picker"]//button[starts-with(@id,"routedatepicker")]'

    date_to_click_xp1 = '//*[@id="drawer-dialog"]/div/div[2]/div/div/div[2]/div/div/div[1]/div/div[3]/div/div[1]/div[1]/div/div/div/div[@data-date="'
    date_to_click_xp2 = '//*[@id="drawer-dialog"]/div/div[2]/div/div/div[2]/div/div/div[1]/div/div[3]/div/div[2]/div[1]/div/div/div/div[@data-date="'

    # xpath to parse results
    departure_time_xp = './/span[1]/span[2]'
    arrival_time_xp = './/span[2]/span[2]'

    times_towards_xp = '/html/body/div[3]/div[2]/main/div[1]/div[2]/div[3]/div[2]/div/div/div/div/div[1]/div/div/div[3]/div/div/div[2]/div/div[2]/div[2]/div/ul/li/div/div/div/button/span[1]'
    times_backwards_xp = '/html/body/div[3]/div[2]/main/div[1]/div[2]/div[3]/div[2]/div/div/div/div/div[2]/div/div/div[2]/div/div/div[2]/div/div[2]/div[2]/div/ul/li/div/div/div/button/span[1]'

    prices_towards_xp = '/html/body/div[3]/div[2]/main/div[1]/div[2]/div[3]/div[2]/div/div/div/div/div[1]/div/div/div[3]/div/div/div[2]/div/div[2]/div[2]/div/ul/li/div/div/div/button/span[2]/span[2]/span[4]/span[1]/span[2]'

    prices_backwards_xp='/html/body/div[3]/div[2]/main/div[1]/div[2]/div[3]/div[2]/div/div/div/div/div[2]/div/div/div[2]/div/div/div[2]/div/div[2]/div[2]/div/ul/li/div/div/div/button/span[2]/span[2]/span[4]/span[1]/span[2]'
