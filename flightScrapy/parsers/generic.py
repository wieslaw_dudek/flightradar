# -*- coding: utf-8 -*-
from selenium.webdriver.common.keys import Keys
import time
from abc import abstractclassmethod
from selenium.webdriver.common.action_chains import ActionChains
from ..items import FlightItem

def overrides(interface_class):
    def overrider(method):
        assert(method.__name__ in dir(interface_class))
        return method
    return overrider


class GenericHandler:
    @property
    def response(self):
        return self._response

    @response.setter
    def response(self, value):
        self._response = value

    @property
    def driver(self):
        return self._driver

    @driver.setter
    def driver(self, value):
        self._driver=value

    @property
    def returning_date(self):
        return self._returning_date

    @returning_date.setter
    def returning_date(self, value):
        self._returning_date=value

    @property
    def departure_date(self):
        return self._departure_date

    @departure_date.setter
    def departure_date(self, value):
        self._departure_date=value

    @property
    def departure(self):
        return self._departure

    @departure.setter
    def departure(self, value):
        self._departure=value

    @property
    def arrival(self):
        return self._arrival

    @arrival.setter
    def arrival(self, value):
        self._arrival=value

    @property
    def search_input_id(self):
        return self._search_input_id

    @search_input_id.setter
    def search_input_id(self, value):
        self._search_input_id = value

    @property
    def send_enter_before_setting_dates(self):
        return self._send_enter_before_setting_dates

    @send_enter_before_setting_dates.setter
    def send_enter_before_setting_dates(self, value):
        self._send_enter_before_setting_dates = value

    def close_last_tab(self):
        if (len(self.driver.window_handles) == 2):
            self.driver.switch_to.window(window_name=self.driver.window_handles[0])
            self.driver.close()
            self.driver.switch_to.window(window_name=self.driver.window_handles[-1])

    def get_page(self):
        self.driver.get(self.response.url)
        time.sleep(2)

    def _click(self, to_click_xp, scroll_to=False):
        to_click_po = self.driver.find_element_by_xpath(to_click_xp)
        if scroll_to:
            self.driver.execute_script("return arguments[0].scrollIntoView();", to_click_po)
        to_click_po.click()

    def _send_keys(self, where, keys, enter=True):
        target_po = self.driver.find_element_by_xpath(where)
        target_po.clear()
        target_po.send_keys(keys)
        if enter:
            target_po.send_keys(Keys.ENTER)

    def _get_times(self, element):
        departure_time_po = element.find_element_by_xpath(self.departure_time_xp)
        ActionChains(self.driver).move_to_element(departure_time_po).perform()

        arrival_time_po = element.find_element_by_xpath(self.arrival_time_xp)
        ActionChains(self.driver).move_to_element(arrival_time_po).perform()
        return departure_time_po.text, arrival_time_po.text

    def _store_results(self,where, times_po, returning, prices_towards_po):
        for i, element in enumerate(times_po):
            departure_time, arrival_time = self._get_times(element)
            where.append({'search_input_id': self.search_input_id,
                          'returning': returning,
                          'departure_time': departure_time,
                          'arrival_time': arrival_time,
                          'price': prices_towards_po[i].text.replace(" zł", "").replace(",", ".").replace(" ", "")})

    def _read_results(self):
        times_towards_po = self.driver.find_elements_by_xpath(self.times_towards_xp)
        times_backwards_po = self.driver.find_elements_by_xpath(self.times_backwards_xp)
        prices_towards_po = self.driver.find_elements_by_xpath(self.prices_towards_xp)
        prices_backwards_po = self.driver.find_elements_by_xpath(self.prices_backwards_xp)

        print(len(times_towards_po))

        flights_towards = []
        flights_backwards = []
        results = FlightItem()
        results['flights_towards'] = flights_towards
        results['flights_backwards'] = flights_backwards

        self._store_results(flights_towards, times_towards_po, False, prices_towards_po)
        self._store_results(flights_backwards, times_backwards_po, True, prices_backwards_po)

        return results

    @abstractclassmethod
    def _get_date_from(self):
        pass

    @abstractclassmethod
    def _get_date_to(self):
        pass

    @abstractclassmethod
    def _set_date(self, date, date_xp):
        pass

    def parse(self):

        self.get_page()
        try:
            if self.to_close_xp: self._click(self.to_close_xp)
        except Exception as ex:
            print(ex)

        if self.two_sides_xp: self._click(self.two_sides_xp)

        if self.send_enter_before_setting_dates:
            self._send_keys(self.departure_station_xp, self.departure)
            self._send_keys(self.arrival_station_xp, self.arrival)

        if self.two_sides_xp: self._click(self.two_sides_xp)

        self._set_date(self.departure_date, self._get_date_from())
        time.sleep(1)
        self._set_date(self.returning_date, self._get_date_to())

        if not self.send_enter_before_setting_dates:
            self._send_keys(self.departure_station_xp, self.departure)
            self._send_keys(self.arrival_station_xp, self.arrival)

        time.sleep(2)
        self._click(self.submit_button_xp)

        #wizzair opens new ad tab so close it
        self.close_last_tab()

        time.sleep(10)
        self.driver.save_screenshot('screen.png')
        results = self._read_results()
        time.sleep(2)
        self.driver.close()
        self.driver.quit()
        return results
