from .generic import GenericHandler, overrides
import time
from selenium.webdriver.common.action_chains import ActionChains

class WizzairHandler(GenericHandler):
    carrier = "Wizzair"

    def _try_to_find_xpath(self, xp):
        try:
            po = self.driver.find_element_by_xpath(xp)
        except:
            po = None
        return po

    @overrides(GenericHandler)
    def _set_date(self, date, xp):
        self._click(xp)
        time.sleep(2)

        date_arr = date.split('-')
        cal_xp = self.calendar_xp + str(int(date_arr[1].lstrip('0')) - 1) + ']'
        po = self._try_to_find_xpath(cal_xp)
        while not po:
            self._click(self.date_next_xp)
            po = self._try_to_find_xpath(cal_xp)
            time.sleep(2)
        self._click(self.select_xp + date_arr[2].lstrip('0') + '"]/button')

    @overrides(GenericHandler)
    def _get_date_from(self):
        return self.date_from_xp

    @overrides(GenericHandler)
    def _get_date_to(self):
        return self.date_to_xp

    @overrides(GenericHandler)
    def _get_times(self, element):
        ActionChains(self.driver).move_to_element(element).perform()
        info = element.text
        times = info.split("\n")[1]
        times_arr = times.split(' ')

        departure_time = times_arr[0]
        arrival_time = times_arr[1]

        return departure_time, arrival_time

    @overrides(GenericHandler)
    def _read_results(self):
        time.sleep(20)
        return super()._read_results()

    # xpath to localize
    to_close_xp = None
    departure_station_xp = '//*[@id="search-departure-station"]'
    arrival_station_xp = '//*[@id="search-arrival-station"]'
    submit_button_xp = '//*[@id="flight-search"]/div/div/div[3]/form/div[4]/button'
    two_sides_xp = None

    date_from_xp = '//*[@id="search-departure-date"]'
    date_next_xp = '//*[@id="flight-search"]/div/div/div[3]/form/fieldset[1]/div[2]/div[3]/div[1]/div[2]/div/button'
    date_to_xp = '//*[@id="search-return-date"]'
    calendar_xp = '//*[@id="flight-search"]/div/div/div[3]/form/fieldset[1]/div[2]/div[3]/div[1]/div[1]/div/div[1]/select/option[@selected and @value='
    select_xp = '//*[@id="flight-search"]/div/div/div[3]/form/fieldset[1]/div[2]/div[3]/div[1]/div[1]/table/tbody/tr/td[@data-day="'

    # xpath to parse results
    times_towards_xp = '//*[@id="fare-selector-outbound"]/div[1]/table/tbody[position()>4]/tr/td[1]'
    times_backwards_xp = '//*[@id="fare-selector-return"]/div[1]/table/tbody[position()>4]/tr/td[1]'

    prices_towards_xp = '//*[@id="fare-selector-outbound"]/div[1]/table/tbody[position()>4]/tr/td[3]/div/div[1]/label/div[2]'
    prices_backwards_xp = '//*[@id="fare-selector-return"]/div[1]/table/tbody[position()>4]/tr/td[3]/div/div[1]/label/div[2]'
