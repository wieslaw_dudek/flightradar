import scrapy
import time
from selenium import webdriver
import sqlite3
import os.path
from ..parsers.ryanair import RyanairHandler
from ..parsers.easyjet import EasyJetHandler
from ..parsers.wizzair import WizzairHandler
import MySQLdb

class FlightsSpider(scrapy.Spider):
    name = "flightScrapy"

    @staticmethod
    def get_handler(carrier_no):
        return [RyanairHandler(), WizzairHandler(), EasyJetHandler()][carrier_no]

    def _init_chrome_driver(self):
        base_dir = os.path.dirname(os.path.abspath(__file__))
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--start-maximized')
        chrome_options.add_argument("--disable-gpu")
        chrome_options.add_argument('--ignore-certificate-errors')
        chrome_options.add_argument('--ignore-ssl-errors')
        chrome_options.add_experimental_option('prefs', {'intl.accept_languages': 'pl,pl_PL'})

        self.driver = webdriver.Chrome(os.path.join(base_dir, "../../chromedriver"),chrome_options=chrome_options)

    def __init__(self):
        super().__init__()

        #FOR SQLlite
        base_dir = os.path.dirname(os.path.abspath(__file__))
        db_path = os.path.join(base_dir, "../../db.sqlite3")
        self.log(db_path)

        self._init_chrome_driver()

        #FOR SQLLite
        self.connection = sqlite3.connect(db_path)
        #FOR MySql
        # self.connection =MySQLdb.connect(host="sql9.freesqldatabase.com",
        #                 user="",
        #                 passwd="",
        #                 db="",charset='utf8', init_command='SET NAMES UTF8')


    def start_requests(self):
        urls = [
            'https://www.ryanair.com/pl/pl/',
            'https://wizzair.com/pl-pl',
            'https://www.easyjet.com/pl'
        ]
        cursor = self.connection.cursor()
        cursor.row_factory = sqlite3.Row
        #FOR sqllite
        rows = cursor.execute('SELECT * FROM flightRadar_searchinput WHERE departure_date>=CURRENT_DATE and active=1').fetchall()
        #FOR Mysql
        # number_of_rows=cursor.execute('SELECT carrier_id,departure,arrival,departure_date,returning_date,id FROM flightRadar_searchinput WHERE departure_date>=CURRENT_DATE and active=1')
        #rows=cursor.fetchall()
        for row in rows:
            #FOR sqllite
            meta = {'carrier': row['carrier_id']-1, 'departure': row['departure'],'arrival': row['arrival'],
                   'departure_date': row['departure_date'], 'returning_date': row['returning_date'],
                   'search_input_id': row['id']}
            url = urls[row['carrier_id'] - 1]
            #FOR mysql
            # meta = {'carrier': row[0] - 1, 'departure': row[1], 'arrival': row[2],
            #         'departure_date': row[3].strftime('%Y-%m-%d'), 'returning_date': row[4].strftime('%Y-%m-%d'),
            #         'search_input_id': row[5]}
            # url = urls[row[0]-1]

            self.log("meta:%s" % meta)
            self.log("url:%s" % url)
            try:
                yield scrapy.Request(url=url, callback=self.parse, meta=meta)
            except Exception as inst:
                print(inst)

    def __del__(self):
        self.driver.quit()
        self.connection.close()

    def close(self, spider):
        self.driver.quit()

    def spider_detail(self, spider):
        self.driver.close()

    def parse(self, response):
        print("Next task:")
        print(response)
        self._init_chrome_driver()

        handler = self.get_handler(response.meta["carrier"])
        handler.response = response
        handler.driver = self.driver
        handler.departure = response.meta["departure"]
        handler.arrival = response.meta["arrival"]
        handler.departure_date = response.meta["departure_date"]
        handler.returning_date = response.meta["returning_date"]
        handler.search_input_id = response.meta['search_input_id']
        # workaround for wizzair which woould hang for a while
        handler.send_enter_before_setting_dates = response.meta["carrier"] != 1
        try:
          self.driver.set_window_size(1535,771)
        except:
          pass

        try:
            res = handler.parse()
            self.log(res)
            time.sleep(2)
            yield res
        except Exception as ex:
            print(ex)
