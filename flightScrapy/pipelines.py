# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://doc.scrapy.org/en/latest/topics/item-pipeline.html

from scrapy.exceptions import DropItem
import sqlite3
import os.path
import MySQLdb

class FlightSavePipeline(object):
    def process_item(self, item, spider):
        spider.log('Well, here is an Item: %s.' % item)

        try:
            for flightOption in item["flights_towards"]+item["flights_backwards"]:
                self.cursor.execute("""INSERT INTO flightRadar_flightdata(
                    created_at,departure_time,arrival_time,price,search_input_id,returning)
                    values(CURRENT_TIMESTAMP,:departure_time,:arrival_time,:price,:search_input_id,:returning)
                    """,flightOption)
                # self.cursor.execute("""INSERT INTO flightRadar_flightdata(
                #     created_at,departure_time,arrival_time,price,search_input_id,returning)
                #     values(CURRENT_TIMESTAMP,'%s','%s',%s,%d,%d)
                #     """%(flightOption['departure_time'],flightOption['arrival_time'],flightOption['price'],flightOption['search_input_id'],int(flightOption['returning'])))

            self.connection.commit()
        except Exception as ex:
            print(ex)
        return item

    def __init__(self):
        BASE_DIR = os.path.dirname(os.path.abspath(__file__))
        db_path = os.path.join(BASE_DIR, "../db.sqlite3")

        #FOR sqllite
        self.connection = sqlite3.connect(db_path)

        #FOR mysql
        # self.connection =MySQLdb.connect(host="sql9.freesqldatabase.com",
        #                 user="",
        #                 passwd="",
        #                 db="")
        self.cursor = self.connection.cursor()

    def __del__(self):
        self.connection.close()
