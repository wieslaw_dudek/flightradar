import time
from selenium import webdriver
chrome_options = webdriver.ChromeOptions()
chrome_options.add_argument('--headless')
chrome_options.add_argument('--user-data-dir=/tmp/something')
chrome_options.add_argument("--disable-gpu")
chrome_options.add_argument("window-size=1920x1080")

driver = webdriver.Chrome('./chromedriver',chrome_options=chrome_options)  # Optional argument, if not specified will search path.
driver.get('http://www.google.com/xhtml');
time.sleep(5) # Let the user actually see something!
search_box = driver.find_element_by_name('q')
search_box.send_keys('ChromeDriver')
search_box.submit()
time.sleep(5) # Let the user actually see something!
driver.quit()

# chrome_options = webdriver.ChromeOptions()
# chrome_options.add_argument('--headless')
# chrome_options.add_argument('--user-data-dir=/tmp/something')
# chrome_options.add_argument("--disable-gpu")
# chrome_options.add_argument("window-size=1920x1080")
# chrome_options.binary_location='./chromedriver'
# chrome_options.add_argument('--no-sandbox') # required when running as root user. otherwise you would get no sandbox errors.
# driver = webdriver.Chrome(executable_path='./chromedriver',chrome_options=chrome_options,
#   service_args=['--verbose', '--log-path=/tmp/chromedriver.log'])
# driver.get('http://www.google.com/xhtml');
# time.sleep(5) # Let the user actually see something!
# search_box = driver.find_element_by_name('q')
# search_box.send_keys('ChromeDriver')
# search_box.submit()
# time.sleep(5) # Let the user actually see something!
# driver.quit()

