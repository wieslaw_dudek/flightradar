#!/usr/bin/env python
import os
import sys
import numpy as np
from sklearn.metrics import recall_score, precision_score, accuracy_score
def custom_score_fun(y_test, y_pred):
    """
    Custom function is used as evaluation method for the model which penalizes it when
    * there was not "to buy" recommendation at all and
    * next in the given order puts pressure on recall of "to buy" instances,
    * then accuracy and
    * recall of "not to buy" instances at the end.
    """
    fn0 = recall_score(y_true=y_test, y_pred=y_pred, pos_label=0)
    fp1 = precision_score(y_true=y_test, y_pred=y_pred, pos_label=1)
    fn1 = recall_score(y_true=y_test, y_pred=y_pred, pos_label=1) / 1000
    fp0 = precision_score(y_true=y_test, y_pred=y_pred, pos_label=0) / 1000
    main_score = ((fp1 if fn0 == 0 else fn0) + accuracy_score(y_true=y_test, y_pred=y_pred) / 100 + (fn1 if fp0 == 0 else fp0))/1.011
    if np.count_nonzero(y_pred) == 0 and np.count_nonzero(y_test) > 0:
        return 0
    else:
        return main_score


if __name__ == "__main__":
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "flightSite.settings")
    try:
        from django.core.management import execute_from_command_line
    except ImportError as exc:
        raise ImportError(
            "Couldn't import Django. Are you sure it's installed and "
            "available on your PYTHONPATH environment variable? Did you "
            "forget to activate a virtual environment?"
        ) from exc
    execute_from_command_line(sys.argv)
