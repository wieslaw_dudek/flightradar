# FlightRadar and Recommendation System -  To buy or not to buy? - supervised classification problem

### Introduction 
Airlines strategy for dynamic tickets pricing is maintained due to the fact that number
of seats to sell changes over the time and they need to maximize its revenue.

The purpose of this project is to study how far we can predict airlines prices change over the time and use it to advice passangers when to buy the ticket or in other words just recommend to buy their ticket or wait.

### Application
To achieve the goal a simple [django application @ http://web-flightradar.1d35.starter-us-east-1.openshiftapps.com/](http://web-flightradar.1d35.starter-us-east-1.openshiftapps.com/) working on Openshift architecture has been created allowing to enter departure/arrival destinations with the dates for the following airlines: Wizzair,Ryanair,Easyjet.

Based on the entered criteria system will scrap minimal prices for the selected destinations from the airline website on daily basis. Next using machine learning models will put its recommendation on the price plot. Historical recommendations in red will indicate wrong decisions based on the new available data, green - "good" ones so far. 

#### Best model prediction example
![Predictions - best model](https://bitbucket.org/wieslaw_dudek/flightradar/raw/master/prediction.png)

#### Voting predition example
![Predictions - voting](https://bitbucket.org/wieslaw_dudek/flightradar/raw/master/prediction_vote.png)

#### Refiting predition example
![Predictions - fit](https://bitbucket.org/wieslaw_dudek/flightradar/raw/master/prediction_fit.png)

Docker images is stored: https://hub.docker.com/r/wdudek/flightradar

### Model

Models are trained on single flights. The other promising option to train on multiple flights from one segment/vendor requires more data and some extra information about segments which was not avalable here. Training on all flights was tested but it gave worse results which make sense as policy should differ in such cases. 

The following models modes have been implemented:
* BEST_EU/US_PREDICT - Each model trained on individual flights is tested on selected flight. One with the best results of prediction is used to classify current price. A similar flight should fit e.g. in the same segment or policy etc.
* ALL_EU/US_VOTING - Each model trained on individual flights is tested on selected flight. Voting classifier is built of models with score above 80% of custom metric is used to classify current price. Similar flights should fit but some worse models can interfere predictions.
* BEST_EU/US_FIT_AND_PREDICT - similar to BEST_EU/US_PREDICT but trained again on flight we want to predict. Actually "buy!" indicator will not be available during prediction and also it is an information leak as training dataset will have some partial information we want to predict i.e. indicator indicates the change in price till departure and should not be used on the same flight). We are expecting overfitting behaviour here.

### Features selection
The following features have been selected:
 
+ number of days left until departure, 
+ the current day of the week
+ the fare price on the previous 7 days normalized by its avarage to be comparable between flights. 

When scrapping flights data there is no guarantee to have a price on selected day/days common for all other flights to be able compare them. The best we can do when comparing flight prices (influences today's price) when we have only data from 7 days back is to use average and normalize them.   

Although it would be good to have it is difficult to get real amount of unsold seats. This is because in the reservation system it is actually minimum of seats available which means "at least" x seats left. Also the publicly available fare data does not include consolidator and corporate tickets. Therefore, because of that uncertainty, it would be of limited use for training models.

### Classification
There are two classes for each price (for flight and day): 1 - "buy!" or 0 - "wait!".  
The "buy!" is recommended if a model predicts a drop in price in the future below 5% of current price (i.e. it does not make sense to wait and take a risk not buying the ticket at all) or "wait!" otherwise   

### Model evaluation
More accuracy does not usually mean better model as for passanger it might be important to minimize the risk not to buy ticket at all or wait too long and buy too expensive ticket. 
Custom function is used as evaluation method for the model which penalizes it when there was not "to buy" recommandation at all and next in the given order puts pressure on recall of "to buy" instances, then accuracy and recall of "not to buy" instances at the end. This is arbitrary selected requirement and could be changed.

Additionally we want to ensure that we do not recommend "not to buy" if predicted decrease in price is less than 5% of current price.

### Data collection
Flight data are being collected using two strategies. European flight segments minimal prices for the selected destinations are scrapped from the airlines websites on daily basis.
USA flight segment data where scraped from [cheapair](https://www.cheapair.com/when-to-buy-flights/to-philadelphia-phl) site.
Cheapair does not present the historical information in text format, but only in the form of a fare chart. To extract fare data about a segment the graph data from html document has been parsed and feed to custom python parser to transform and scale it. 

### Other
Other models have been analyzed in separate notebooks as time serries or regression and failed as it looks like there is no possibility to learn and predict ticket prices just before departure based on historical data of the same flight only. To find such patterns we need much more data but also look at similar time period before departure and similar flights /same segment, class etc./ and learn on it.

We can further improve models by using more time consuming classifiers as random forests, neural networks or boosting algoritms.  

### Notebooks 
* [To buy or not to buy - supervised classification problem](https://bitbucket.org/wieslaw_dudek/flightradar/src/master/FlightPrices_BuyOrNot.ipynb?viewer=nbviewer)
* [Time series analysis problem](https://bitbucket.org/wieslaw_dudek/flightradar/src/master/FlightPrices_TimeSeries.ipynb?viewer=nbviewer)
* [Regression problem](https://bitbucket.org/wieslaw_dudek/flightradar/src/master/FlightPrices_Regresion.ipynb?viewer=nbviewer)

