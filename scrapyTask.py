from subprocess import call
import time
import os

def scrapy_job():
   call(['scrapy','crawl','flightScrapy'])
   print("Doing some cleanup")
   call(['rm','-rf','/app/core.*'])
   print("Cleanup done!")
   call(["ls","-rtl"])

while True:
  try: 
    scrapy_job()
  except Exception as ex:
    print(ex)
  freq=int(os.environ['FREQ'])
  print("Sleeping for %d minutes"%freq)
  time.sleep(freq*60)
