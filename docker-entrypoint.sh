#!/usr/bin/env bash
cd /app
Xvfb :99 -screen 0 800x600x16 &
sleep 2
echo "Setting scrapy cron"
nohup python /app/scrapyTask.py > /tmp/flightscrapy.log &
echo "Starting Xvfb"
echo "Starting django app"
/app/flightradar.sh
echo "Ended django app"
