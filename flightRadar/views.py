from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic import ListView, DetailView
from .models import SearchInput, FlightData
from datetime import date
from django.urls import reverse_lazy
from django import forms
from django.forms.utils import ErrorList
from django.core.paginator import Paginator,PageNotAnInteger, EmptyPage
from django.db import connection
from .utils import dict_fetch_all, flight_data_sql


class SearchInputForm(forms.ModelForm):
    class Meta:
        model = SearchInput
        fields = '__all__'
        widgets = {
            'departure_date': forms.DateInput(format='%Y-%m-%d', attrs={'class': 'datepicker form-control', 'placeholder': 'Select departure date'}),
            'returning_date': forms.DateInput(format='%Y-%m-%d',
                                              attrs={'class': 'datepicker form-control', 'placeholder': 'Select returning date'}),
            'departure': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Enter departure'}),
            'arrival': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Enter arrival'}),
            'active': forms.CheckboxInput(attrs={'class': 'form-control'}),
            'carrier': forms.Select(attrs={'class': 'form-control', 'placeholder': 'Enter carrier'}),
        }

    def clean(self):
        try:
            dpt = self.cleaned_data['departure_date']
        except:
            dpt = date.today()
        try:
            ret = self.cleaned_data['returning_date']
        except:
            ret = date.today()
        if dpt < date.today():
            msg = 'The departure date cannot be in the past'
            self._errors['departure_date'] = ErrorList([msg])
            del self.cleaned_data['departure_date']
        else:
            if dpt > ret:
                msg = 'The departure date cannot be later than the end date.'
                self._errors['returning_date'] = ErrorList([msg])
                del self.cleaned_data['returning_date']
        return self.cleaned_data


class SearchInputCreate(CreateView):
    model = SearchInput
    form_class = SearchInputForm
    initial = {'departure_date': date.today(), 'returning_date': date.today()}


class SearchInputUpdate(UpdateView):
    model = SearchInput
    form_class = SearchInputForm

    def form_valid(self, form):
        response = super(SearchInputUpdate, self).form_valid(form)
        self.object.flightdata_set.all().delete()
        return response


class SearchInputDelete(DeleteView):
    model = SearchInput
    success_url = reverse_lazy('searchinput_list')


class SearchInputListView(ListView):
    model = SearchInput
    paginate_by = 4


class SearchInputDetailView(DetailView):
    model = SearchInput
    flightdata_paginate_by = 8

    def get_context_data(self, **kwargs):
        context = super(SearchInputDetailView, self).get_context_data(**kwargs)

        flightdata_page = self.request.GET.get("page")
        flightdata = self.object.flightdata_set.all()
        flightdata_paginator = Paginator(flightdata, self.flightdata_paginate_by)
        try:
            flightdata_page_obj = flightdata_paginator.page(flightdata_page)
        except (PageNotAnInteger, EmptyPage):
            flightdata_page_obj = flightdata_paginator.page(1)

        context["page_obj"] = flightdata_page_obj
        context["flightData"] = flightdata
        context["is_paginated"]=True
        return context


class FlightDataListView(ListView):
    model = FlightData
    context_object_name = 'flightData'
    paginate_by = 8
    template_name = 'flightRadar/flightdata_list.html'

    def get_queryset(self):
        cursor = connection.cursor()
        cursor.execute(flight_data_sql())
        queryset = dict_fetch_all(cursor)
        return queryset

