from django.http import HttpResponse
import matplotlib
matplotlib.use("SVG")
from matplotlib import pylab
import PIL
import PIL.Image
from io import BytesIO
from django.db import connection
from .utils import flight_data_sql, getPredictions, predict, PredictMode
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from matplotlib.dates import AutoDateLocator, DateFormatter,date2num

def showVotePredictions(id,**pk):
    return showPredictions(id, PredictMode.ALL_EU_VOTING, pk)

def showTrainPredictions(id,**pk):
    return showPredictions(id, PredictMode.BEST_EU_FIT_AND_PREDICT, pk)

def showBestPredictions(id, **pk):
    return showPredictions(id, PredictMode.BEST_EU_PREDICT, pk)

def showPredictions(id,mode,pk):
    results = getPredictions(pk)
    if mode==-1:
        plt.subplots(1, 3,figsize=(40, 20))
    else:
        plt.figure(figsize=(15, 8))
    for j,prediction in enumerate(PredictMode):
        if (mode == -1 or prediction==mode) and j<3:
            if mode==-1: plt.subplot(1,3, j+1)
            for result in results:
                dataset=result[2]
                plt.plot(dataset, label=result[0])
                plt.xlabel('Date')
                plt.ylabel('Price (zł)')
                ax = plt.gca()
                ax.xaxis.set_major_locator(AutoDateLocator())
                ax.xaxis.set_major_formatter(DateFormatter('%Y-%m-%d'))
                plt.legend(loc='upper left', bbox_to_anchor=(0, 1.2))
                plt.xticks(rotation=70)
                plt.title('Prices and Recommendations %s'%prediction)
                predictions=predict(result,prediction)

                for i, x in enumerate(reversed(predictions)):
                    #print(i, x, dataset.index[-i - 1])
                    if x:
                        if i==0:
                            fc="orange"
                        elif np.all(dataset.iloc[-i-1] - 0.05 * dataset.iloc[-i-1] < dataset.values[-i-1:]):
                            fc="green"
                        else:
                            fc="red"
                        plt.annotate(
                            "Buy!",
                            xy=(dataset.index[-i-1], dataset.iloc[-i-1]), xytext=(-20, 20),
                            textcoords='offset points', ha='right', va='bottom', size=7 if i!=0 else 10,
                            bbox=dict(boxstyle='round,pad=0.5', fc=fc, alpha=1),
                            arrowprops=dict(arrowstyle='->', connectionstyle='arc3,rad=0'))

    plt.grid()
    plt.tight_layout(rect=[0,0,1,0.9])

    # Store image in a string buffer
    buffer = BytesIO()
    canvas = plt.get_current_fig_manager().canvas
    canvas.draw()
    plt.savefig(buffer)
    plt.close("all")
    connection.close()

    # Send buffer in a http response the the browser with the mime type image/png set
    return HttpResponse(buffer.getvalue(), content_type="image/png")

def showStatsForQuery(id,**pk):

    df = pd.read_sql(flight_data_sql(pk), connection)

    returning = df[df["returning"] == 1].reset_index()
    price_returning = returning["price"]
    towards = df[df["returning"] == 0].reset_index()
    price_towards = towards["price"]

    plt.subplots(2, 2, figsize=(10, 10))
    plt.subplot(2, 2, 1)
    plt.boxplot([price_towards, price_returning], positions=[2, 1],
                labels=['Price box - towards', 'Price box - returning'], vert=False)
    plt.title('Price distribution')

    plt.subplot(2, 2, 2)
    Y1 = towards[["created_at", "price"]]
    X = date2num([i for i in towards["created_at"]])
    plt.plot(X, towards["price"],'x', color="blue", label="towards")
    X = date2num([i for i in returning["created_at"]])
    plt.plot(X, returning["price"],'x', color="red",label="returning")
    ax = plt.gca()
    ax.grid()
    ax.xaxis.set_major_locator(AutoDateLocator())
    ax.xaxis.set_major_formatter(DateFormatter('%Y\n%m\n%d'))
    plt.xlabel('Checked at')
    plt.ylabel('Price (zł)')
    plt.legend(loc='best')
    plt.title('Flight prices')

    plt.subplot(2, 2, 3)
    X = towards['price'].astype(np.float_)
    Y = date2num([i for i in towards['departure_date']])
    plt.hist2d(X, Y)
    plt.colorbar()
    plt.xlabel('Price (zł)')
    plt.ylabel('Date')
    ax = plt.gca()
    ax.yaxis.set_major_locator(AutoDateLocator())
    ax.yaxis.set_major_formatter(DateFormatter('%Y-%m-%d'))
    plt.title('Histogram - towards')

    plt.subplot(2, 2, 4)
    X = returning['price'].astype(np.float_)
    Y = date2num([i for i in returning['returning_date']])
    plt.hist2d(X, Y)
    plt.colorbar()
    plt.xlabel('Price (zł)')
    plt.ylabel('Date')
    ax = plt.gca()
    ax.yaxis.set_major_locator(AutoDateLocator())
    ax.yaxis.set_major_formatter(DateFormatter('%Y-%m-%d'))
    plt.title('Histogram - returning')

    plt.tight_layout()
    # Store image in a string buffer
    buffer = BytesIO()
    canvas = plt.get_current_fig_manager().canvas
    canvas.draw()
    plt.savefig(buffer)
    plt.close("all")
    connection.close()

    # Send buffer in a http response the the browser with the mime type image/png set
    return HttpResponse(buffer.getvalue(), content_type="image/png")

def showStats(request):

    df = pd.read_sql(flight_data_sql(), connection)

    df["arr"] = df["arr"].apply(lambda x: x.split("-")[0])
    df["dpt"] = df["dpt"].apply(lambda x: x.split("-")[0])
    df["arrival"] = df["arrival"].apply(lambda x: x.split("-")[0])
    df["departure"] = df["departure"].apply(lambda x: x.split("-")[0])

    wizzair_prices = df[df["name"] == "Wizzair"]
    ryanair_prices = df[df["name"] == "Ryanair"]
    easyjet_prices = df[df["name"] == "EasyJet"]

    wizzair_prices_oneside = wizzair_prices[['dpt', 'arr', 'price']].groupby(['dpt', 'arr']).agg(np.mean)
    ryanair_prices_oneside = ryanair_prices[['dpt', 'arr', 'price']].groupby(['dpt', 'arr']).agg(np.mean)
    easyjet_prices_oneside = easyjet_prices[['dpt', 'arr', 'price']].groupby(['dpt', 'arr']).agg(np.mean)

    wizzair_prices_twosides = wizzair_prices[['returning', 'departure', 'arrival', 'price']].groupby(
        ['departure', 'arrival']).agg(np.mean)
    ryanair_prices_twosides = ryanair_prices[['returning', 'departure', 'arrival', 'price']].groupby(
        ['departure', 'arrival']).agg(np.mean)
    easyjet_prices_twosides = easyjet_prices[['returning', 'departure', 'arrival', 'price']].groupby(
        ['departure', 'arrival']).agg(np.mean)

    res = pd.concat([easyjet_prices_oneside, ryanair_prices_oneside, wizzair_prices_oneside], axis=1)
    res_twosides = pd.concat(
        [easyjet_prices_twosides[['price']], ryanair_prices_twosides[['price']], wizzair_prices_twosides[['price']]],
        axis=1)

    plt.figure()
    plt.subplots(2, 2, figsize=(20, 10))
    plt.subplot(1, 2, 1)
    ax = res.plot(kind="bar", color=['r', 'g', 'b'], ax=plt.gca())
    ax.grid()
    ax.legend(["EasyJet","Ryanair","Wizzair"])
    ax.set_ylabel('Price (zł)')
    ax.set_xlabel('Departure,Arrival')
    ax.set_title("Price devided by carriers")

    plt.subplot(1, 2, 2)
    ax = res_twosides.plot(kind="bar", color=['r', 'g', 'b'], ax=plt.gca())
    ax.grid()
    ax.legend(["EasyJet", "Ryanair", "Wizzair"])
    ax.set_ylabel('Price (zł)')
    ax.set_xlabel('Departure,Arrival and back')
    ax.set_title("Price devided by carriers - both sides")

    plt.tight_layout()
    # Store image in a string buffer
    buffer = BytesIO()
    canvas = plt.get_current_fig_manager().canvas
    canvas.draw()
    plt.savefig(buffer)
    plt.close("all")
    connection.close()

    # Send buffer in a http response the the browser with the mime type image/png set
    return HttpResponse(buffer.getvalue(), content_type="image/png")


