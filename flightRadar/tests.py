from django.test import TestCase, Client
from .models import Carrier,SearchInput
from django.urls import reverse

class CarrierTestCase(TestCase):
    def setUp(self):
       Carrier.objects.create(name="carrier_name")

    def test_stringnify(self):
        """Carrier name should be correctly printed"""
        carrier = Carrier.objects.get(name="carrier_name")
        self.assertEqual(carrier.__str__(), 'carrier_name')

class SearchInputTestCase(TestCase):
    def setUp(self):
        carrier=Carrier.objects.create(name="carrier_name")
        SearchInput.objects.create(id=1,departure="AMS",arrival="JFK",departure_date='2018-01-01',returning_date="2018-08-08",carrier=carrier)

    def test_get_absolute_url(self):
        author=SearchInput.objects.get(id=1)
        self.assertEquals(author.get_absolute_url(),'/flightRadar/searchinput/')

class SearchInputTest(TestCase):
    def setUp(self):
        number_of_queries = 7
        carrier=Carrier.objects.create(name="carrier_name")
        for i in range(1,number_of_queries+1):
            SearchInput.objects.create(id=i,departure="AMS",arrival="JFK",departure_date='2018-01-%d'%i,returning_date="2018-08-%d"%i,carrier=carrier)
        self.client = Client()

    def test_view_url_exists_at_desired_location(self):
        resp = self.client.get('/flightRadar/searchinput/')
        self.assertEqual(resp.status_code, 200)

    def test_view_url_accessible_by_name(self):
        resp = self.client.get(reverse('searchinput_list'))
        self.assertEqual(resp.status_code, 200)

    def test_view_uses_correct_template(self):
        resp = self.client.get(reverse('searchinput_list'))
        self.assertEqual(resp.status_code, 200)

        self.assertTemplateUsed(resp, 'flightRadar/searchinput_list.html')

    def test_pagination_is_eight(self):
        resp = self.client.get(reverse('searchinput_list'))
        self.assertEqual(resp.status_code, 200)
        self.assertTrue('is_paginated' in resp.context)
        self.assertTrue(resp.context['is_paginated'] == True)
        self.assertTrue(len(resp.context['searchinput_list']) == 4)

    def test_lists_all_authors(self):
        # Get second page and confirm it has (exactly) remaining 3 items
        resp = self.client.get(reverse('searchinput_list') + '?page=2')
        self.assertEqual(resp.status_code, 200)
        self.assertTrue('is_paginated' in resp.context)
        self.assertTrue(resp.context['is_paginated'] == True)
        self.assertTrue(len(resp.context['searchinput_list']) == 3)

#TODO do more tests