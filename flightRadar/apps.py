from django.apps import AppConfig


class FlightRadarConfig(AppConfig):
    name = 'flightRadar'
