from django.db import connection
import numpy as np
import pandas as pd
from sklearn.model_selection import GridSearchCV
from sklearn.metrics import make_scorer,accuracy_score, precision_score, recall_score, confusion_matrix, roc_auc_score, f1_score
from sklearn.model_selection import train_test_split, KFold
import warnings
from collections import namedtuple
import glob
from sklearn.metrics import classification_report
from sklearn.pipeline import Pipeline, make_pipeline
from sklearn.tree import DecisionTreeClassifier
from sklearn.naive_bayes import BernoulliNB, MultinomialNB, GaussianNB
from sklearn.linear_model import LogisticRegression
from sklearn.neighbors import KNeighborsClassifier
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.discriminant_analysis import QuadraticDiscriminantAnalysis
from sklearn.preprocessing import StandardScaler
from sklearn.ensemble import VotingClassifier, RandomForestClassifier
from mlxtend.classifier import EnsembleVoteClassifier
from sklearn.externals import joblib

import os

from enum import Enum

#*********************************************************************************************
# Utility functions

class PredictMode(Enum):
    BEST_EU_PREDICT = 0
    BEST_EU_FIT_AND_PREDICT = 1
    ALL_EU_VOTING = 2
    BEST_US_PREDICT = 3
    BEST_US_FIT_AND_PREDICT = 4
    ALL_US_VOTING = 5

#Used from https://machinelearningmastery.com/convert-time-series-supervised-learning-problem-python/
def series_to_supervised(data, n_in=1, n_out=1, dropnan=True):
    """
    Frame a time series as a supervised learning dataset.
    Arguments:
        data: Sequence of observations as a list or NumPy array.
        n_in: Number of lag observations as input (X).
        n_out: Number of observations as output (y).
        dropnan: Boolean whether or not to drop rows with NaN values.
    Returns:
        Pandas DataFrame of series framed for supervised learning.
    """
    n_vars = 1 if type(data) is list else data.shape[1]
    df = pd.DataFrame(data)
    cols, names = list(), list()
    # input sequence (t-n, ... t-1)
    for i in range(n_in, 0, -1):
        cols.append(df.shift(i))
        names += [('var%d(t-%d)' % (j+1, i)) for j in range(n_vars)]
    # forecast sequence (t, t+1, ... t+n)
    for i in range(0, n_out):
        cols.append(df.shift(-i))
        if i == 0:
            names += [('var%d(t)' % (j+1)) for j in range(n_vars)]
        else:
            names += [('var%d(t+%d)' % (j+1, i)) for j in range(n_vars)]
    # put it all together
    agg = pd.concat(cols, axis=1)
    agg.columns = names
    # drop rows with NaN values
    if dropnan:
        agg.dropna(inplace=True)
    return agg


def getSeriesWithDaysBeforeDepartureAndHistoryPrices(dataset, dptDt, lags=7):
    """
    Prepare futures for forecasting
    Arguments:
        dataset: rough dataset in pandas Series format
        dptDt: departure date as datetime64
        lags: Number of observations as output (y).
    Returns:
        (X,y) tuple with numpy arrays ready to fit model
    """
    df = pd.DataFrame()
    df['t'] = [x for x in dataset]
    dataframe = series_to_supervised(df, lags)

    datatime_vect = np.vectorize(np.datetime64)
    try:
        date1 = datatime_vect(dataset.index.values)
        date2 = datatime_vect(dptDt)
    except:
        date1 = dataset.index.values
        date2 = dptDt
    daysBDpt = ((date2 - date1) + 1).astype('timedelta64[D]')[lags:]
    dow = [pd.Timestamp(i).dayofweek for i in date1][lags:]
    dataframe.insert(0, 'daysBDpt', pd.Series(daysBDpt.astype(int), index=dataframe.index))
    dataframe.insert(1, 'dow', pd.Series(dow, index=dataframe.index))
    array = dataframe.values
    # split into input and output
    X = array[:, 0:-1]
    eps = 1e-10

    lag_mean = array[:, 2:-1].mean(axis=1)
    # normalize prices to avg before prices
    for i in range(2, lags + 2):
        array[:, i] = array[:, i] / lag_mean

        # Create to buy indicator
    # we want to ensure that we do not recommend "not to buy" if predicted decrease in price is less than 5% of current price
    per = 0.05
    y = [int(np.all(array[i, -1] - per * array[i, -1] < array[i + 1:, -1])) \
         for i in range(len(array[:, -1]))]
    return (X, y)

def custom_score_fun(y_test, y_pred):
    """
    Custom function is used as evaluation method for the model which penalizes it when
    * there was not "to buy" recommendation at all and
    * next in the given order puts pressure on recall of "to buy" instances,
    * then accuracy and
    * recall of "not to buy" instances at the end.
    """
    fn0 = recall_score(y_true=y_test, y_pred=y_pred, pos_label=0)
    fp1 = precision_score(y_true=y_test, y_pred=y_pred, pos_label=1)
    fn1 = recall_score(y_true=y_test, y_pred=y_pred, pos_label=1) / 1000
    fp0 = precision_score(y_true=y_test, y_pred=y_pred, pos_label=0) / 1000
    main_score = ((fp1 if fn0 == 0 else fn0) + accuracy_score(y_true=y_test, y_pred=y_pred) / 100 + (fn1 if fp0 == 0 else fp0))/1.011
    if np.count_nonzero(y_pred) == 0 and np.count_nonzero(y_test) > 0:
        return 0
    else:
        return main_score

#*********************************************************************************************
# Model selection utilities

random_state=123
KN=3
models=[
    {"name": "LogisticRegression", "pipeline": make_pipeline(StandardScaler(with_mean=False),LogisticRegression(random_state=random_state))},
    {"name": "MultinomialNB", "pipeline": MultinomialNB()},
    {"name": "DecisionTreeClassifier", "pipeline": DecisionTreeClassifier(random_state=random_state)},
    {"name": "KNeighborsClassifier","pipeline":KNeighborsClassifier(KN)},
    {"name": "LinearDiscriminantAnalysis", "pipeline":LinearDiscriminantAnalysis()},
    {"name": "QuadraticDiscriminantAnalysis","pipeline":QuadraticDiscriminantAnalysis()}
   ]

params=[
    {"logisticregression__class_weight": ['balanced',None],
     "logisticregression__penalty":["l1","l2"],"logisticregression__C":[0.001,0.01,0.1,1,10,100,1000]},
    {},
    {
     "class_weight": ['balanced',None],
     "criterion":["gini","entropy"],
     'splitter':['best','random'],
     'min_samples_split':[2,5,7,10],
     'max_depth':[2,5,10,100,200,300]},
    {},
    {},
    {}
   ]

def selectModel(models, params, X_train, X_test, y_train, y_test, splits=3):
    """
    Train and select best model using train,test sets
    Returns tuple with best model and voting classifier with set of the best classifiers at score 0.8 and more
    """
    print(y_train)
    print(y_test)
    warnings.filterwarnings("ignore")
    model_name = None
    best_model, best_custom_score, best_rep_score = None, -1, -1
    seed = 123
    try:
        my_cv = KFold(n_splits=splits, random_state=seed)
    except:
        # too few data
        my_cv = KFold(n_splits=2, random_state=seed)

    print(my_cv)
    voting_models = []
    for model, param in zip(models, params):
        try:
            print(model['name'])
            custom_scorer = make_scorer(custom_score_fun, greater_is_better=True)

            gs = GridSearchCV(model['pipeline'], param, scoring=custom_scorer, cv=my_cv, refit=True, n_jobs=1)
            gs.fit(X_train, y_train)

            y_pred = gs.best_estimator_.predict(X_test)
            score = custom_score_fun(y_test, y_pred)
            print("True:", y_test, "Predicted:", y_pred)
            print("Custom score: %s" % score)
            rep = classification_report(y_test, y_pred, target_names=['NOT Buy', 'Buy!'])
            if score > 0.8:
                print("Score %s adding to voting_models %s" % (score, model['name']))
                voting_models.append((model['name'], best_model))
            if score > best_custom_score:
                print("Current best score %s model %s" % (score, model['name']))
                best_model, best_custom_score, best_rep_score, model_name = gs, score, rep, model['name']

        except Exception as e:
            print(e)
    print("Score: %s cust_score %s params %s" % (model_name, best_custom_score, best_model.best_params_))
    print("Report %s", best_rep_score)

    if len(voting_models) == 0: voting_models.append(("best", best_model))
    vot = VotingClassifier(estimators=voting_models, voting="soft")
    vot.fit(X_train, y_train)
    print("VOTING score:")
    y_pred = vot.predict(X_test)
    print(custom_score_fun(y_test, y_pred))
    print(classification_report(y_test, y_pred, target_names=['NOT Buy', 'Buy!']))

    return (best_model, vot)

def testmodels(models,params,flight,dptDt,dataset,**opt):
    """
    Splitting data on training and testing part
    Seleting best models
    """
    print(100*'-')
    print(flight, opt)
    n_test_size=int(0.34*len(dataset))
    print(n_test_size)
    X,y=getSeriesWithDaysBeforeDepartureAndHistoryPrices(dataset,dptDt,opt['lags'])
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=n_test_size)

    return selectModel(models,params,X_train, X_test, y_train, y_test,opt['splits'])

#*********************************************************************************************
# Predicion and plotting utilities

def getBestModel(ids, X, y, X_train, y_train, X_test, y_test, mode, verbose=2):
    warnings.filterwarnings("ignore")
    """
    Get best model predictions for given flight based on models saved on disk and prediction mode
    Split on train and test for FIT_AND_TRAINING modes
    For other modes predicting on whole data X,y
    """
    estimators = []
    best_score = -1
    best_model = None
    original_best_model = None

    if mode in [PredictMode.ALL_EU_VOTING,PredictMode.BEST_EU_FIT_AND_PREDICT,PredictMode.BEST_EU_PREDICT]:
        segment="eu"
    else:
        segment="us"


    for f in glob.glob("models/*.%s.rec"%segment):
        fileName = os.path.basename(f)
        flightName = os.path.splitext(f)
        if ids.flight+"."+segment+".rec"==fileName:
            if verbose>0: print("SKIPPED for the same flight")
        else:
            try:
                model = joblib.load(f)
                y_model_pred = model.predict(X_test)
                model_score = custom_score_fun(y_test, y_model_pred)
                if best_score < model_score:
                    best_score, best_model = model_score, model
                if (model_score>0.8):
                    estimators.append(model)
            except Exception as e:
                if verbose>0: print(e)
    if len(estimators)==0:
        estimators.append(best_model)
    if mode==PredictMode.BEST_EU_FIT_AND_PREDICT or mode==PredictMode.BEST_US_FIT_AND_PREDICT:
        if verbose>1: print("Best model - fitted on test data of target flight")
        try:
            best_model.fit(X_train, y_train)
        except Exception as e:
            if verbose>0: print(e)
        best_y_pred = best_model.predict(X_test)
        f_best_score = custom_score_fun(y_test, best_y_pred)
        f1__score = f1_score(y_test, best_y_pred)
        try:
            auc_score=roc_auc_score(y_test, best_y_pred)
        except:
            auc_score=0
        rec_score=recall_score(y_test, best_y_pred)
        acc_score=accuracy_score(y_test, best_y_pred)
        if verbose>0: print("Score:", f_best_score)
        if verbose>1: print("True:", y_test)
        if verbose>1: print("Predicted:", best_y_pred)
        return (best_model.predict(X)==1,f_best_score,f1__score,auc_score,rec_score,acc_score)

    if mode == PredictMode.BEST_EU_PREDICT or mode == PredictMode.BEST_US_PREDICT:
        if verbose>1: print("Best model - original fitted")
        best_org_y_pred = best_model.best_estimator_.predict(X)
        best_org_score = custom_score_fun(y, best_org_y_pred)
        f1__score = f1_score(y, best_org_y_pred)
        try:
            auc_score=roc_auc_score(y, best_org_y_pred)
        except:
            auc_score=0
        rec_score=recall_score(y, best_org_y_pred)
        acc_score=accuracy_score(y, best_org_y_pred)
        if verbose>0: print("Score:", best_org_score)
        if verbose>1: print("True:", y)
        if verbose>1: print("Predicted:", best_org_y_pred)
        return (best_org_y_pred==1,best_org_score,f1__score,auc_score,rec_score,acc_score)

    if mode == PredictMode.ALL_EU_VOTING or mode == PredictMode.ALL_US_VOTING:
        if verbose>1: print('Voting hard')
        v = EnsembleVoteClassifier(clfs=estimators, voting="hard", refit=False)
        v.fit(None, np.array([0, 1]))
        try:
            y_pred = v.predict(X)
            voting_score = custom_score_fun(y, y_pred)
            f1__score = f1_score(y, y_pred)
            try:
                auc_score=roc_auc_score(y, y_pred)
            except:
                auc_score=0
            rec_score=recall_score(y, y_pred)
            acc_score=accuracy_score(y, y_pred)
            if verbose>0: print("Score:", voting_score)
            if verbose>1: print("True:", y)
            if verbose>1: print("Predicted:", y_pred)
            return (y_pred==1,voting_score,f1__score,auc_score,rec_score,acc_score)
        except Exception as e:
            if verbose>0: print(e)

def predict(ids_,mode):
    """
    Return predictions for all days in steps:
    1) Prepare features and split by train and test data
    2) Select best model in given mode
    3) Return predictions
    """
    lags=7
    splits=5

    n_test_size=int(0.34*len(ids_.data))
    print("Predict flight: %s Test size: %s mode: %s"%(ids_.flight,n_test_size,mode))
    if n_test_size>2:
        X_, y_ = getSeriesWithDaysBeforeDepartureAndHistoryPrices(ids_.data, ids_.dpt_dt, lags)
        X_train_, X_test_, y_train_, y_test_ = train_test_split(X_, y_, test_size=n_test_size, shuffle=False)

        pred,_,_,_,_,_=getBestModel(ids_, X_, y_, X_train_, y_train_, X_test_, y_test_,mode)
        return pred
    else:
        return []

def dict_fetch_all(cursor):
    desc = cursor.description
    return [
            dict(zip([col[0] for col in desc], row))
            for row in cursor.fetchall()
    ]


def fillGaps(dataset):
    df1 = dataset.to_frame()
    df1.index = pd.to_datetime(df1.index)
    df2 = pd.DataFrame([], index=[str(d).strip() for d in np.arange(dataset.index.min(),
                                                                    dataset.index.max(), 1)])
    df2.index.astype(np.datetime64)
    df3 = df1.merge(df2, how="outer", left_index=True, right_index=True)

    df3 = df3.fillna(method='ffill')
    return df3[0]


def getPredictions(id,flightsOnly=False):
    sql = """SELECT
                              carrier.name carrier,
                              CASE WHEN data.returning=1 THEN query.arrival ELSE query.departure END as dpt,
                              CASE WHEN data.returning=1 THEN query.departure ELSE query.arrival END as arr,
                              CASE WHEN data.returning=1 THEN query.returning_date ELSE query.departure_date END as dpt_dt,
                              CASE WHEN data.returning=1 THEN query.departure_date ELSE query.returning_date END as arr_dt,
                              departure_time,
                              arrival_time,
                              data.returning,
                              price, created_at
            FROM flightRadar_carrier as carrier
            INNER JOIN flightRadar_searchinput as query
            ON (query.carrier_id=carrier.id)
            INNER JOIN flightRadar_flightdata as data
            ON (query.id=data.search_input_id)"""+\
            ("WHERE query.id="+str(id['pk']) if not flightsOnly else "")

    df = pd.read_sql(sql, connection)

    Flight = namedtuple("Flight", ["flight", "dpt_dt", "data"], verbose=False, rename=False)

    scraped_datasets = []
    grouped = df.groupby(['carrier', 'dpt', 'arr', 'dpt_dt', 'arr_dt', 'departure_time', 'arrival_time'])
    for name, group in grouped:
        flight = ','.join(str(column).replace('0 days ', '') for column in name)
        days = group.groupby([df['created_at'].dt.date])
        XX = []
        yy = []
        for nm, grp in days:
            XX.append(nm)
            yy.append(grp['price'].mean())
        scraped_datasets.append(Flight(dpt_dt=None, flight=flight, data=pd.Series(yy, XX)))

    # FILLING GAPS IN CHECKS WITH PREVIOUS PRICE
    predictions=[]
    for ds in scraped_datasets:
        splited = ds.flight.split(",")
        departure_date = np.datetime64(splited[3])
        dataset=fillGaps(ds.data)
        if not flightsOnly:
            predictions.append((Flight(flight=ds.flight, dpt_dt=departure_date, data=dataset)))
        else:
            predictions.append({"flight":ds.flight,"dataset":dataset, "buy": predict(Flight(flight=ds.flight, dpt_dt=departure_date, data=dataset))})
    return predictions

def flight_data_sql(id=None):
        return """SELECT
                          carrier.name,
                          query.departure_date, data.departure_time,
                          query.departure,
                          query.arrival,
                          CASE WHEN data.returning=1 THEN query.arrival ELSE query.departure END as dpt,
                          CASE WHEN data.returning=1 THEN query.departure ELSE query.arrival END as arr,
                          CASE WHEN data.returning=1 THEN query.returning_date ELSE query.departure_date END as dpt_dt,
                          CASE WHEN data.returning=1 THEN query.departure_date ELSE query.returning_date END as arr_dt,
                          data.arrival_time,
                          data.returning,
                          query.returning_date,
                          price, created_at
                          FROM flightRadar_carrier as carrier
                          INNER JOIN flightRadar_searchinput as query
                          ON (query.carrier_id=carrier.id)
                          INNER JOIN flightRadar_flightdata as data
                          ON (query.id=data.search_input_id)
                          """+("WHERE query.id="+str(id['pk'])+" " if id else "")+"""
                          ORDER BY carrier.name,
                          CASE WHEN data.returning=1 THEN query.returning_date ELSE query.departure_date END,
                          CASE WHEN data.returning=1 THEN query.departure_date ELSE query.returning_date END,
                          CASE WHEN data.returning=1 THEN query.arrival ELSE query.departure END,
                          CASE WHEN data.returning=1 THEN query.departure ELSE query.arrival END,
                          data.returning,
                          query.returning_date,
                          data.departure_time, data.arrival_time,
                          created_at, price"""

