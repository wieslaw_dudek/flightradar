from django.urls import path
from . import views, charts

urlpatterns = [
    path('', views.SearchInputListView.as_view(), name='index'),
    path('searchinput/', views.SearchInputListView.as_view(), name='searchinput_list'),
    path('searchinput/<int:pk>/detail/', views.SearchInputDetailView.as_view(), name='searchinput_detail'),
    path('searchinput/create/', views.SearchInputCreate.as_view(), name='searchinput_create'),
    path('searchinput/<int:pk>/update/', views.SearchInputUpdate.as_view(), name='searchinput_update'),
    path('searchinput/<int:pk>/delete/', views.SearchInputDelete.as_view(), name='searchinput_delete'),
    path('flightdata/', views.FlightDataListView.as_view(), name='flightdata_list'),
    path('showstats4query/<int:pk>/', charts.showStatsForQuery, name='showstats4query'),
    path('showpredictions/best/<int:pk>/', charts.showBestPredictions, name='showbestpredictions'),
    path('showpredictions/vote/<int:pk>/', charts.showVotePredictions, name='showvotepredictions'),
    path('showpredictions/train/<int:pk>/', charts.showTrainPredictions, name='showtrainpredictions'),
    path('showstats', charts.showStats, name='showstats'),
]
