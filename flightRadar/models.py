# -*- coding: utf-8 -*-
from django.db import models
from django.urls import reverse


class Carrier(models.Model):
    name = models.CharField('Carrier name',max_length=200)

    def __str__(self):
        return '%s' % self.name


class SearchInput(models.Model):
    departure = models.CharField('Departure',max_length=100)
    arrival = models.CharField('Arrival',max_length=100)
    departure_date = models.DateField('Departure date')
    returning_date = models.DateField('Returning date')
    carrier = models.ForeignKey(Carrier, on_delete=models.CASCADE)
    active = models.BooleanField("Active", default=False)

    @staticmethod
    def get_absolute_url():
        return reverse('searchinput_list')

    def __str__(self):
        return '{:15} - {:10} \U0001F6EB: {:25} \U0001F6EC: {:25} \U00002600 {}'\
            .format(self.departure_date.strftime("%Y-%m-%d"),
                    self.returning_date.strftime("%Y-%m-%d"),
                    self.departure, self.arrival, self.carrier)

    class Meta:
        ordering = ['departure', 'arrival', 'departure_date', 'returning_date', 'carrier']


class FlightData(models.Model):
    created_at= models.DateTimeField('Created at', auto_now_add=True)
    search_input = models.ForeignKey(SearchInput, on_delete=models.CASCADE)
    departure_time = models.TimeField("Departure time")
    arrival_time = models.TimeField("Arrival time")
    price = models.FloatField("Price")
    returning = models.BooleanField("Returning", default=False)

    class Meta:
        ordering = ['-created_at','returning', 'departure_time','arrival_time','search_input']
