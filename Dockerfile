FROM python:3.6-slim-stretch

# Install essential packages
RUN apt-get update -y \
    && apt-get -y install \
        dumb-init gnupg wget ca-certificates apt-transport-https \
        ttf-wqy-zenhei unzip \
    && rm -rf /var/lib/apt/lists/* /var/cache/apt/*

# Install Chrome Headless Browser
RUN wget -q -O - https://dl.google.com/linux/linux_signing_key.pub | apt-key add - \
    && echo "deb https://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google-chrome.list \
    && apt-get update -y \
    && apt-get -y install google-chrome-unstable \
    && rm /etc/apt/sources.list.d/google-chrome.list \
    && rm -rf /var/lib/apt/lists/* /var/cache/apt/*

# Install prerequisite packages for scrapy
RUN apt-get update -y
RUN apt-get -y install build-essential libssl-dev libffi-dev libxml2-dev libxslt-dev

# Install chromedriver
ARG CHROME_DRIVER_VERSION=2.35
RUN wget -O tmp/chromedriver_linux64.zip https://chromedriver.storage.googleapis.com/$CHROME_DRIVER_VERSION/chromedriver_linux64.zip
RUN unzip tmp/chromedriver_linux64.zip -d tmp \
    && rm tmp/chromedriver_linux64.zip \
    && chmod 755 tmp/chromedriver \
    && mv tmp/chromedriver /usr/bin/chromedriver

# Install util packages
RUN apt-get install -y git nano vim cron

# Install scrapy & selenium
RUN pip install scrapy selenium

# install xvfb
RUN apt-get install -yqq xvfb

# set display port and dbus env to avoid hanging
ENV DISPLAY=:99
ENV DBUS_SESSION_BUS_ADDRESS=/dev/null

#install mysql
RUN apt-get install -y mysql-client
RUN apt-get install -y default-libmysqlclient-dev

#Install python packages
RUN pip install django
RUN pip install matplotlib
RUN pip install Pillow
RUN pip install pandas
RUN pip install scrapy
RUN pip install mysqlclient
RUN pip install apscheduler
RUN pip install mlxtend

# Port to expose
EXPOSE 8000

RUN apt-get install -y tk

# Install flight radar application
RUN mkdir  /app

COPY db.sqlite3 /app
COPY flightRadar /app/flightRadar
COPY flightScrapy /app/flightScrapy
COPY flightSite /app/flightSite
COPY manage.py /app
COPY scrapy.cfg /app
COPY docker-entrypoint.sh /app
COPY scrapyTask.py /app
COPY flightradar.sh /app
COPY models /app/models

# Use installed driver
RUN cp /usr/bin/chromedriver /app

RUN chmod -R 777 /app
RUN echo "tmpfs /dev/shm tmpfs defaults,size=256m 0 0" > /etc/fstab
RUN apt-get install -y procps
# Start django part
CMD ["/bin/bash", "-C", "/app/docker-entrypoint.sh"]

